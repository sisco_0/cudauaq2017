# README #

This repository has been created by Francisco José Solís Muñoz.
The only purpose of this repository is to contain all the examples
created at the CUDA course at Universidad Autónoma de Querétaro
from 19th to 23th of June 2017.

### What is this repository for? ###

* Quick learning CUDA
* Helping people to understand parallel computing
* Demonstrating how to develop at one machine and compiling at other
* Updating